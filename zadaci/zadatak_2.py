import json

with open('covid-data.json') as f:
    data = json.load(f)

february_data = {}
for item in data:
    if '202102' in str(item['date']):
        february_data[item['date']] = {'positive': item['positiveIncrease'],
                                       'hospitalized': item['hospitalizedIncrease']}
print(february_data)

import csv

with open('february_data.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['date', 'positive', 'hospitalized'])
    for date, values in february_data.items():
        writer.writerow([date, values['positive'], values['hospitalized']]) 

import datetime 

def hospitalization_percentage(date_string):
    date = datetime.datetime.strptime(date_string, '%Y%m%d')
    positive_count = 0
    hospitalized_count = 0
    for date_key, values in february_data.items():
        if datetime.datetime.strptime(str(date_key), '%Y%m%d') == date:
            positive_count += values['positive']
            hospitalized_count += values['hospitalized']
            break
    else:
        return None  # ako ne postoji podatak za zadati datum, tj. nije unesen dan u februaru

    percentage = hospitalized_count / positive_count * 100
    return percentage

result = hospitalization_percentage('20210205')
print(result) 
