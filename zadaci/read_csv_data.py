import csv

def read_csv_data(csv_file):
    with open(csv_file,'r') as f:
        reader = csv.DictReader(f)
        data = {}
        for row in reader:
            date = row['date']
            positive = int(row['positive'])
            hospitalized = int(row['hospitalized'])
            data[date] = {
                'positive': positive,
                'hospitalized': hospitalized
            }
        return data

read_csv_data('february_data.csv')