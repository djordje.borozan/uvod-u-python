developers = { "Marshal": ["Company website", "Animal recognition", "Fashion store website"], "Ted": ["Plants watering system", "WHAT", "Animal recognition", "Food recognition"], "Monica": ["NEXT website", "Fashion store website"], "Phoebe": ["WHAT", "Company website", "NEXT website"] }
project_list = [project for projects in developers.values() for project in projects]
print(project_list)
project_set = set(project_list)
print(project_set)
# koristimo set da bi izbjegli ponavljanje elemenata 

projects = {}

# iteriramo kroz svakog programera i projekat koji je radio i dodajemo informaciju u dictionary projekata
for developer, projects_list in developers.items():
    for project in projects_list:
        if project not in projects:
            projects[project] = [developer]
        else:
            projects[project].append(developer)

# ispisujemo dictionary projekata
print(projects)

new_projects = {}

for developer, projects_list in developers.items():
    for project in projects_list:
        new_project_name = f"BIXBIT_{project}" 
        if new_project_name not in new_projects:
            new_projects[new_project_name] = [developer]
        else:
            new_projects[new_project_name].append(developer)

print(new_projects)

def add_developer(name, projects_list):
    developers[name] = projects_list
    return len(developers)

new_length=add_developer("Djordje", ["Data analysis", "Artificial Inteligence", "Machine learning"])
print(new_length)
print(developers) 

def update_developer_info(hours, earnings):
    for developer, projects_list in developers.items():
        updated_projects = []
        for project in projects_list:
            updated_projects.append((project, f"{hours}h", earnings))
        developers[developer] = updated_projects

update_developer_info("240", 5000)
print(developers) 

def print_developers_earnings():
    for developer, projects_list in developers.items():
        total_hours = 0
        total_earnings = 0
        for project_tuple in projects_list:
            hours = project_tuple[1]
            earnings = project_tuple[2]
            total_hours += int(hours[:-1])
            total_earnings += int(earnings)
        earnings_per_hour = total_earnings / total_hours
        print(f"{developer} zaradjuje {earnings_per_hour} eura po satu.")

print_developers_earnings()